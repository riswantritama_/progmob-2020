package ukdw.com.progmob2020.Crud3;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import ukdw.com.progmob2020.Crud2.DosenAddActivity;
import ukdw.com.progmob2020.Crud2.DosenDeleteActivity;
import ukdw.com.progmob2020.Crud2.DosenGetAllActivity;
import ukdw.com.progmob2020.Crud2.DosenUpdateActivity;
import ukdw.com.progmob2020.Crud2.MainDsnActivity;
import ukdw.com.progmob2020.R;

public class MainMkActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_mk);

        Button btnGet = (Button)findViewById(R.id.btnGet);
        Button btnAdd = (Button)findViewById(R.id.btnNew);
        Button btnEdt = (Button)findViewById(R.id.btnUp);
        Button btnDel = (Button)findViewById(R.id.btnDel);

        btnGet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainMkActivity.this, MatkulGetAllActivity.class);
                startActivity(intent);
            }
        });

        btnAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainMkActivity.this, MatkulAddActivity.class);
                startActivity(intent);
            }
        });

        btnEdt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainMkActivity.this, MatkulUpdateActivity.class);
                startActivity(intent);
            }
        });

        btnDel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainMkActivity.this, MatkulDeleteActivity.class);
                startActivity(intent);
            }
        });
    }
}