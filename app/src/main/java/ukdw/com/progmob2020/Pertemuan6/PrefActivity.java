package ukdw.com.progmob2020.Pertemuan6;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ukdw.com.progmob2020.Model.User;
import ukdw.com.progmob2020.Network.GetDataService;
import ukdw.com.progmob2020.Network.RetrofitClientInstance;
import ukdw.com.progmob2020.R;

public class PrefActivity extends AppCompatActivity {

    String isLogin;
    List<User> users;
    ProgressDialog pd;
    SharedPreferences session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pref);

        Button btnPref3 = (Button)findViewById(R.id.btnPref3);

        SharedPreferences pref = PrefActivity.this.getSharedPreferences("pref_file",MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();

        isLogin = pref.getString("isLogin", "0");
        if(isLogin.equals("1")){
            btnPref3.setText("Logout");
        }else{
            btnPref3.setText("Login");
        }

        final EditText un = (EditText)findViewById(R.id.editTeksUser);
        final EditText pass = (EditText)findViewById(R.id.editTeksPass);
        pd = new ProgressDialog(PrefActivity.this);

        btnPref3.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                pd.setTitle("Loading");
                pd.show();

                Intent intent = new Intent(PrefActivity.this, UtsLoginMainActivity.class);
                Bundle b = new Bundle();
                b.putString("help_string", un.getText().toString());
                intent.putExtras(b);

                if(un.getText().toString().length() == 0){
                    un.setError("Masukkan Username");
                }else if(pass.getText().toString().length()==0){
                    pass.setError("Masukkan Password");
                }else{
                    Toast.makeText(getApplicationContext(),"Berhasil Login", Toast.LENGTH_LONG).show();
                    startActivity(intent);
                }


                isLogin = pref.getString("isLogin", "0");
                if(isLogin.equals("0")){
                    editor.putString("isLogin", "1");
                    btnPref3.setText("Logout");
                }else{
                    editor.putString("isLogin","0");
                    btnPref3.setText("Login");
                }

                editor.commit();
            }
        });
    }
}